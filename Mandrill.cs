﻿using Mandrill;
using Mandrill.Models;
using Mandrill.Requests.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Main.Services
{
    public class MandrillEmailService 
    {
        MandrillApi _api;

        public MandrillEmailService()
        {
            _api = new MandrillApi(ConfigurationManager.AppSettings["Mandrill-ApiKey"]);
        }

        private Boolean Testing()
        {
            var sendToAddress = ConfigurationManager.AppSettings["SMTP-SENDTOTEST"];
            return !String.IsNullOrWhiteSpace(sendToAddress);
        }

        private String ParseEmail(String emailAddress)
        {
            if (Testing())
            {
                emailAddress = ConfigurationManager.AppSettings["SMTP-SENDTOTEST"];
            }
            return emailAddress;
        }

        private EmailMessage CreateMandrilMsg(String toEmail, String toName, dynamic data)
        {
            var email = ParseEmail(toEmail);

            var msg = new EmailMessage
            {

                To = new List<EmailAddress> {
                new EmailAddress {  Email = email,   Name =  toName } },
                FromEmail = "website@venturefounders.co.uk",
                FromName = "VentureFounders",
            };

            foreach (var dataItem in (Dictionary<String, Object>)Dyn2Dict(data))
            {
                msg.AddRecipientVariable(email, dataItem.Key, dataItem.Value.ToString());
            }

            return msg;
        }

        private Dictionary<String, Object> Dyn2Dict(dynamic dynObj)
        {
            var dictionary = new Dictionary<string, object>();
            foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(dynObj))
            {
                object obj = propertyDescriptor.GetValue(dynObj);
                dictionary.Add(propertyDescriptor.Name, obj);
            }
            return dictionary;
        }

        public async Task<bool> Send(string template, Recipient recipient, dynamic data)
        {
            Boolean result = false;

            try
            {
                EmailMessage msg = CreateMandrilMsg(recipient.Email, recipient.Name, data);
                var request = new SendMessageTemplateRequest(msg, template);
                List<EmailResult> mandrillResponse = await _api.SendMessageTemplate(request); //.SendMessageAsync(msg, template, null);

                result = (mandrillResponse[0].Status == EmailResultStatus.Sent);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return result;
        }

    }
}