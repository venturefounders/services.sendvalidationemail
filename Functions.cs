﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Web.Main.Services;
using Newtonsoft.Json;

namespace Services.SendValidationEmail
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.

        public class userModel {
            public String Email { get; set; }
            public String FirstName { get; set; }
            public String LastName { get; set; }
            public String PinCode { get; set;  }
            public Boolean EmailVerified { get; set; }
        }

        public static async Task SendEmailValidation(
            [ServiceBusTrigger("NewRegisteredUser", "SendEmailValidation")] String data, 
            TextWriter log)
        {
            userModel user = JsonConvert.DeserializeObject<userModel>(data);

            Console.WriteLine("SEND VALIDATE EMAIL: {0} {1}" , user.Email,user.PinCode);

            if (!user.EmailVerified)
            {
                var emailService = new MandrillEmailService();

                await emailService.Send("validateemail",
                    new Mandrill.Models.Recipient
                    {
                        Email = user.Email,
                        Name = String.Format("{0} {1}", user.FirstName, user.LastName)
                    },
                    new
                    {
                        RegisteredEmail = user.Email,
                        PinCode = user.PinCode,
                        BaseUrl = "http://localhost:52562"
                    }
                );
                Console.WriteLine("SENT", user.Email);

            }
            else
            {
                Console.WriteLine("User already verified password to {0}", user.Email);

            }
            
        }
    }
}
